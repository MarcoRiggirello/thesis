#!/usr/bin/env python

import ROOT

ROOT.gInterpreter.Declare("""
        double Ehash(ROOT::Math::PtEtaPhiMVector mu, ROOT::Math::PtEtaPhiMVector mu1, ROOT::Math::PtEtaPhiMVector mu2) {
            ROOT::Math::Boost boost;
            auto MU = mu1 + mu2;
            auto beta = MU.BoostToCM();
            boost.SetComponents(beta);
            auto muhash = boost(mu);
            return muhash.E();
        }
        double gEhash(ROOT::Math::PtEtaPhiEVector mu, ROOT::Math::PtEtaPhiEVector mu1, ROOT::Math::PtEtaPhiEVector mu2) {
            ROOT::Math::Boost boost;
            auto MU = mu1 + mu2;
            auto beta = MU.BoostToCM();
            boost.SetComponents(beta);
            auto muhash = boost(mu);
            return muhash.E();
        }
""")

def Rkinevars(df, NN=True):
    """
    Defines new columns in the given RDataFrame
    for the kinematic variables used for the 
    analysis (and others, for convenience).
    If NN=True uses the neural network corrected
    pt for the Bc meson, otherwise it uses the 
    'manual' correction defined as in references.
    """
    # MBc = 6.2751 GeV
    # Mmu = 0.10566 GeV
    if NN:
        df = df.Define("Bc_pt_correct", "bc_pt_predicted")
    else:
        df = df.Define("Bc_pt_correct", "bcCorrected_pt")
    # generation of particles 4-momenta
    df = df.Define("Bc", "ROOT::Math::PtEtaPhiMVector(Bc_pt_correct, Bc_eta, Bc_phi, 6.2751)")\
            .Define("mu", "ROOT::Math::PtEtaPhiMVector(Bc_mu_pt, Bc_mu_eta, Bc_mu_phi, 0.10566)")\
            .Define("mu1", "ROOT::Math::PtEtaPhiMVector(Bc_jpsi_mu1_pt, Bc_jpsi_mu1_eta, Bc_jpsi_mu1_phi, 0.10566)")\
            .Define("mu2", "ROOT::Math::PtEtaPhiMVector(Bc_jpsi_mu2_pt, Bc_jpsi_mu2_eta, Bc_jpsi_mu2_phi, 0.10566)")
    # other 4-momenta of interest
    df = df.Define("mm", "Bc - mu - mu1 - mu2")\
            .Define("Q", "Bc - mu1 - mu2")
    # the kinematic variables of interest
    df = df.Define("m2", "mm.M2()")\
            .Define("Q2", "Q.M2()")\
            .Define("Ehash", "Ehash(mu, mu1, mu2)")\
            .Define("pt_miss", "Bc_pt_correct - Bc_jpsi_mu1_pt - Bc_jpsi_mu2_pt - Bc_mu_pt")
    return df

def Rgen_kinevars(df):
    """
    Same as Rkinevars but for the generated variables.
    """
    # generation of particles 4-momenta
    df = df.Define("gen_Bc", "ROOT::Math::PtEtaPhiEVector(gen_b_pt, gen_b_Eta, gen_b_Phi, gen_b_E)")\
            .Define("gen_mu", "ROOT::Math::PtEtaPhiEVector(gen_mu_pt, gen_mu_Eta, gen_mu_Phi, gen_mu_E)")\
            .Define("gen_mu1", "ROOT::Math::PtEtaPhiEVector(gen_jpsi_mu1_pt, gen_jpsi_mu1_Eta, gen_jpsi_mu1_Phi, gen_jpsi_mu1_E)")\
            .Define("gen_mu2", "ROOT::Math::PtEtaPhiEVector(gen_jpsi_mu2_pt, gen_jpsi_mu2_Eta, gen_jpsi_mu2_Phi, gen_jpsi_mu2_E)")
    # other 4-momenta of interest
    df = df.Define("gen_mm", "gen_Bc - gen_mu - gen_mu1 - gen_mu2")\
            .Define("gen_Q", "gen_Bc - gen_mu1 - gen_mu2")
    # the kinematic variables of interest
    df = df.Define("gen_m2", "gen_mm.M2()")\
            .Define("gen_Q2", "gen_Q.M2()")\
            .Define("gen_Ehash", "gEhash(gen_mu, gen_mu1, gen_mu2)")\
            .Define("gen_pt_miss", "gen_b_pt - gen_jpsi_mu1_pt - gen_jpsi_mu2_pt - gen_mu_pt")
    return df
