#!/usr/bin/env python

import numpy as np
import pandas as pd
import ROOT

from scipy.optimize import basinhopping, OptimizeWarning
import warnings
warnings.simplefilter("ignore", category=OptimizeWarning)

from Rkinevars import *
from Rcuts import *

# import data
muon_file = "results-muon_channel.root"
tau_file = "results-tau_channel.root"

muon_rdf = ROOT.RDataFrame('tree', muon_file)
tau_rdf = ROOT.RDataFrame('tree', tau_file)

# compute the kinematic variables of interest
muon_rdf = Rkinevars(muon_rdf)
tau_rdf = Rkinevars(tau_rdf)

muon_df = Rroot2pandas(muon_rdf)
tau_df = Rroot2pandas(tau_rdf)

# brute force optimization it's not feasible in this case, 
# there are 149990400 combinations of cuts values here.
# At the speed of 100 combinations/second, this optimization
# will take about 17 days to complete. 
print("DR/R OPTIMIZATION")

def f(x, mdf=muon_df, tdf=tau_df):
    """
    return the relative error of R(J/psi)
    """
    muon_cuts = x[0:4]
    tau_cuts = x[4:8]
    R, DR = RJpsi(mdf, tdf, muon_cuts, tau_cuts)
    if R==0:
        r = 1000.0
    else:
        r = DR/R
    return r

x0 = [5.75, 8.8, 0.0, 4.0, 0.0, 0.0, 2.646, 3.9]
bnds = ((0.,9.),(0.,11.),(0.,5.),(0.,15.),(0.,9.),(0.,11.),(0.,5.),(0.,15.))
kwargs = {"method": "Powell", "options": {"xtol": 0.05}, "bounds": bnds} 

res = basinhopping(f, x0, minimizer_kwargs=kwargs)
print(res)

muon_cuts = res.x[0:4]
tau_cuts = res.x[4:8]
Npm, Nrm = mcut(muon_df, muon_cuts)
Npt, Nrt = tcut(tau_df, tau_cuts)

np.set_printoptions(precision=4)
print("muon cuts:\t", muon_cuts)
print("muon eff:\t", Npm/(Npm+Nrm))
print("muon s:\t", mscore(muon_df, tau_df, muon_cuts))
print("tau cuts:\t", tau_cuts)
print("tau eff:\t", Npt/(Npt+Nrt))
print("tau s:\t", tscore(muon_df, tau_df, tau_cuts))
print("R(J/psi) expected (R,DR):\t", RJpsi(muon_df, tau_df, res.x[0:4], res.x[4:8]))

