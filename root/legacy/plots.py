#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import ROOT

from kinevars import *

# import data in to a pandas data frame
muon_file = "results-muon_channel.root"
tau_file = "results-tau_channel.root"

muon_rdf = ROOT.RDataFrame('tree', muon_file)
tau_rdf = ROOT.RDataFrame('tree', tau_file)

muon_df = root2pandas(muon_rdf)
tau_df = root2pandas(tau_rdf)
    
# manipulation
bc_pt_correct(muon_df)#, NN=True)
bc_pt_correct(tau_df)#, NN=True)
 
kinevars(muon_df)
kinevars(tau_df)

gen_kinevars(muon_df)
gen_kinevars(tau_df)
    
# plotting the results
tau_vs_muon_m2 = pd.DataFrame()
tau_vs_muon_m2["muon"] = muon_df["m2"]
tau_vs_muon_m2["tau"] = tau_df["m2"]
tau_vs_muon_m2.plot.hist(bins=60, alpha=0.6)
plt.title("missing mass squared - reco muon vs tau channel")
plt.xlabel("[GeV^2/c^4]")

tau_vs_muon_Q2 = pd.DataFrame()
tau_vs_muon_Q2["muon"]= muon_df["Q2"]
tau_vs_muon_Q2["tau"] = tau_df["Q2"]
tau_vs_muon_Q2.plot.hist(bins=60, alpha=0.6)
plt.title("Q2 - reco muon vs tau channel")
plt.xlabel("[GeV^2/c^4]")

tau_vs_muon_Ehash = pd.DataFrame()
tau_vs_muon_Ehash["muon"]= muon_df["Ehash"]
tau_vs_muon_Ehash["tau"] = tau_df["Ehash"]
tau_vs_muon_Ehash.plot.hist(bins=60, alpha=0.6)
plt.title("Ehash - reco muon vs tau channel")
plt.xlabel("[GeV]")

tau_vs_muon_pt_miss = pd.DataFrame()
tau_vs_muon_pt_miss["muon"]= muon_df["pt_miss"]
tau_vs_muon_pt_miss["tau"] = tau_df["pt_miss"]
tau_vs_muon_pt_miss.plot.hist(bins=60, alpha=0.6)
plt.title("missing pt - reco muon vs tau channel")
plt.xlabel("[GeV/c]")

tau_vs_muon_gen_Ehash = pd.DataFrame()
tau_vs_muon_gen_Ehash["muon"]= muon_df["gen_Ehash"]
tau_vs_muon_gen_Ehash["tau"] = tau_df["gen_Ehash"]
tau_vs_muon_gen_Ehash.plot.hist(bins=60, alpha=0.6)
plt.title("Ehash - gen muon vs tau channel")
plt.xlabel("[GeV]")

tau_vs_muon_gen_pt_miss = pd.DataFrame()
tau_vs_muon_gen_pt_miss["muon"]= muon_df["gen_pt_miss"]
tau_vs_muon_gen_pt_miss["tau"] = tau_df["gen_pt_miss"]
tau_vs_muon_gen_pt_miss.plot.hist(bins=60, alpha=0.6)
plt.title("missing pt - gen muon vs tau channel")
plt.xlabel("[GeV/c]")

tau_vs_muon_gen_m2 = pd.DataFrame()
tau_vs_muon_gen_m2["muon"]= muon_df["gen_m2"]
tau_vs_muon_gen_m2["tau"] = tau_df["gen_m2"]
tau_vs_muon_gen_m2.plot.hist(bins=60, alpha=0.6)
plt.title("missing mass squared - gen muon vs tau channel")
plt.xlabel("[GeV^2/c^4]")

tau_vs_muon_gen_Q2 = pd.DataFrame()
tau_vs_muon_gen_Q2["muon"]= muon_df["gen_Q2"]
tau_vs_muon_gen_Q2["tau"] = tau_df["gen_Q2"]
tau_vs_muon_gen_Q2.plot.hist(bins=60, alpha=0.6)
plt.title("Q2 - gen muon vs tau channel")
plt.xlabel("[GeV^2/c^4]")

gen_vs_reco_muon_m2 = pd.DataFrame() 
gen_vs_reco_muon_m2["gen"] = muon_df["gen_m2"]
gen_vs_reco_muon_m2["reco"] = muon_df["m2"]
gen_vs_reco_muon_m2.plot.hist(bins=60, alpha=0.6)
plt.title("gen vs reco missing mass sq - muon channel")
plt.xlabel("[GeV^2/c^4]")

gen_vs_reco_tau_m2 = pd.DataFrame() 
gen_vs_reco_tau_m2["gen"] = tau_df["gen_m2"]
gen_vs_reco_tau_m2["reco"] = tau_df["m2"]
gen_vs_reco_tau_m2.plot.hist(bins=60, alpha=0.6)
plt.title("gen vs reco missing mass sq - tau channel")
plt.xlabel("[GeV^2/c^4]")

gen_vs_reco_muon_Q2 = pd.DataFrame() 
gen_vs_reco_muon_Q2["gen"] = muon_df["gen_Q2"]
gen_vs_reco_muon_Q2["reco"] = muon_df["Q2"]
gen_vs_reco_muon_Q2.plot.hist(bins=60, alpha=0.6)
plt.title("gen vs reco Q2 - muon channel")
plt.xlabel("[GeV^2/c^4]")

gen_vs_reco_tau_Q2 = pd.DataFrame() 
gen_vs_reco_tau_Q2["gen"] = tau_df["gen_Q2"]
gen_vs_reco_tau_Q2["reco"] = tau_df["Q2"]
gen_vs_reco_tau_Q2.plot.hist(bins=60, alpha=0.6)
plt.title("gen vs reco Q2 - tau channel")
plt.xlabel("[GeV^2/c^4]")

plt.show()

