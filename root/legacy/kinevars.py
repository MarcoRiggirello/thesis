#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import ROOT

def root2pandas(input_rdf):
    """
    takes a ROOT.RDataFrame as input and
    returns a pandas data frame with only the branches 
    that will be used for this analysis.
    """
    ndf = input_rdf.AsNumpy(columns=[
        "gen_b_pt", "gen_b_Eta", "gen_b_Phi", "gen_b_E", "gen_b_pt",
        "gen_jpsi_mu1_pt", "gen_jpsi_mu1_Eta", "gen_jpsi_mu1_Phi", "gen_jpsi_mu1_E",
        "gen_jpsi_mu2_pt", "gen_jpsi_mu2_Eta", "gen_jpsi_mu2_Phi", "gen_jpsi_mu2_E",
        "gen_mu_pt", "gen_mu_Eta", "gen_mu_Phi", "gen_mu_E",
        "bc_E", "Bc_pt", "Bc_eta", "Bc_phi",
        "bc_pt_predicted",
        "mu_E", "Bc_mu_pt", "Bc_mu_eta", "Bc_mu_phi", 
        "mu1_E", "Bc_jpsi_mu1_pt", "Bc_jpsi_mu1_eta", "Bc_jpsi_mu1_phi", 
        "mu2_E", "Bc_jpsi_mu2_pt", "Bc_jpsi_mu2_eta", "Bc_jpsi_mu2_phi"])
    return pd.DataFrame(ndf)

### RECONSTRUCTED VARIABLES ### 

def mum(r): 
    """
    takes as input a row of a pandas df and 
    returns the invariant mass of the three muon sys
    """
    mu = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["Bc_mu_pt"], r["Bc_mu_eta"], r["Bc_mu_phi"], r["mu_E"])
    mu1 = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["Bc_jpsi_mu1_pt"], r["Bc_jpsi_mu1_eta"], r["Bc_jpsi_mu1_phi"], r["mu1_E"])
    mu2 = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["Bc_jpsi_mu2_pt"], r["Bc_jpsi_mu2_eta"], r["Bc_jpsi_mu2_phi"], r["mu2_E"])
    mm = mu + mu1 + mu2
    return mm.M()

def bc_pt_correct(df, NN=False):
    """
    creates a new column in the given pandas 
    data frame with the corrected pt of the Bc.
    If NN=True the Neural Network corrected pt is
    used instead of the 'manual' correction.
    """
    if NN:
        df["Bc_pt_correct"] = df["bc_pt_predicted"]
    else:
        Bc_invmass = 6.2751 # GeV
        df["3mu_invmass"] = df.apply(mum, axis=1)
        df["Bc_pt_correct"] = Bc_invmass*df["Bc_pt"]/df["3mu_invmass"]

def m2(r):
    """
    takes as input a row of a pandas df and returns
    the missing mass squared defined as below.
    """
    Bc = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_pt_correct"], r["Bc_eta"], r["Bc_phi"], 6.2751)
    mu = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_mu_pt"], r["Bc_mu_eta"], r["Bc_mu_phi"], 0.10566)
    mu1 = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_jpsi_mu1_pt"], r["Bc_jpsi_mu1_eta"], r["Bc_jpsi_mu1_phi"], 0.10566)
    mu2 = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_jpsi_mu2_pt"], r["Bc_jpsi_mu2_eta"], r["Bc_jpsi_mu2_phi"], 0.10566)
    mm = Bc - mu - mu1 - mu2
    return mm.M2()

def Q2(r):
    """
    Takes as input a row of a pandas df and returns
    the missing momentum squared defined as below.
    """
    Bc = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_pt_correct"], r["Bc_eta"], r["Bc_phi"], 6.2751)
    mu1 = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_jpsi_mu1_pt"], r["Bc_jpsi_mu1_eta"], r["Bc_jpsi_mu1_phi"], 0.10566)
    mu2 = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_jpsi_mu2_pt"], r["Bc_jpsi_mu2_eta"], r["Bc_jpsi_mu2_phi"], 0.10566)
    Q = Bc - mu1 - mu2
    return Q.M2()

def Ehash(r):
    """
    Takes as input a row of a pandas df and returns
    the energy of the unpaired muon in the rest frame of
    the others two muons.
    """
    mu = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_mu_pt"], r["Bc_mu_eta"], r["Bc_mu_phi"], 0.10566)
    mu1 = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_jpsi_mu1_pt"], r["Bc_jpsi_mu1_eta"], r["Bc_jpsi_mu1_phi"], 0.10566)
    mu2 = ROOT.Math.PtEtaPhiMVector.LorentzVector(r["Bc_jpsi_mu2_pt"], r["Bc_jpsi_mu2_eta"], r["Bc_jpsi_mu2_phi"], 0.10566)
    boost = ROOT.Math.Boost.Boost()
    MU = mu1 + mu2
    beta = MU.BoostToCM()
    boost.SetComponents(beta)
    muhash = boost(mu)
    return muhash.E()

def kinevars(df):
    """
    Add the kinematic variables columns to a given pandas
    df, where m2 and Q2 are the missing mass and momentum
    squared, respectively.
    """
    df["m2"] = df.apply(m2, axis=1)
    df["Q2"] = df.apply(Q2, axis=1)
    df["Ehash"] = df.apply(Ehash, axis=1)
    df["pt_miss"] = df["Bc_pt_correct"] - df["Bc_jpsi_mu1_pt"] - df["Bc_jpsi_mu2_pt"] - df["Bc_mu_pt"]

### GENERATED VARIABLES ###

def gen_m2(r):
    """
    Same as m2() but for the generated variables.
    """
    Bc = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_b_pt"], r["gen_b_Eta"], r["gen_b_Phi"], r["gen_b_E"])
    mu = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_mu_pt"], r["gen_mu_Eta"], r["gen_mu_Phi"], r["gen_mu_E"])
    mu1 = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_jpsi_mu1_pt"], r["gen_jpsi_mu1_Eta"], r["gen_jpsi_mu1_Phi"], r["gen_jpsi_mu1_E"])
    mu2 = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_jpsi_mu2_pt"], r["gen_jpsi_mu2_Eta"], r["gen_jpsi_mu2_Phi"], r["gen_jpsi_mu2_E"])
    mm = Bc - mu - mu1 - mu2
    return mm.M2()

def gen_Q2(r):
    """
    Same as Q2() but for the generated variables.
    """
    Bc = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_b_pt"], r["gen_b_Eta"], r["gen_b_Phi"], r["gen_b_E"])
    mu1 = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_jpsi_mu1_pt"], r["gen_jpsi_mu1_Eta"], r["gen_jpsi_mu1_Phi"], r["gen_jpsi_mu1_E"])
    mu2 = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_jpsi_mu2_pt"], r["gen_jpsi_mu2_Eta"], r["gen_jpsi_mu2_Phi"], r["gen_jpsi_mu2_E"])
    Q = Bc - mu1 - mu2
    return Q.M2()

def gen_Ehash(r):
    """
    Same as Ehash() but for the generated variables.
    """
    mu = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_mu_pt"], r["gen_mu_Eta"], r["gen_mu_Phi"], r["gen_mu_E"])
    mu1 = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_jpsi_mu1_pt"], r["gen_jpsi_mu1_Eta"], r["gen_jpsi_mu1_Phi"], r["gen_jpsi_mu1_E"])
    mu2 = ROOT.Math.PtEtaPhiEVector.LorentzVector(r["gen_jpsi_mu2_pt"], r["gen_jpsi_mu2_Eta"], r["gen_jpsi_mu2_Phi"], r["gen_jpsi_mu2_E"])
    boost = ROOT.Math.Boost.Boost()
    MU = mu1 + mu2
    beta = MU.BoostToCM()
    boost.SetComponents(beta)
    muhash = boost(mu)
    return muhash.E()

def gen_kinevars(df):
    """
    Same as kinevars() but for the generated variables.
    """ 
    df["gen_m2"] = df.apply(gen_m2, axis=1)
    df["gen_Q2"] = df.apply(gen_Q2, axis=1)
    df["gen_Ehash"] = df.apply(gen_Ehash, axis=1)
    df["gen_pt_miss"] = df["gen_b_pt"] - df["gen_jpsi_mu1_pt"] - df["gen_jpsi_mu2_pt"] - df["gen_mu_pt"]

