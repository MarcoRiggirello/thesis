#!/usr/bin/env python

import numpy as np
import pandas as pd
import ROOT

from kinevars import *

def pureff(muon_df, tau_df, Nm, Nt, cuts, debug=False):
    
    # columns of the df used for the cuts.
    kin = ["m2", "Q2", "Ehash", "pt_miss"]
    
    # random selection of events.
    if debug:
        edf = muon_df[kin].sample(n=Nm, random_state=2).append(tau_df[kin].sample(n=Nt, random_state=2), ignore_index=True, sort=False)
    else:
        edf = muon_df[kin].sample(n=Nm).append(tau_df[kin].sample(n=Nt), ignore_index=True, sort=False)
    
    # apply the cuts. where the conditions are false
    # the edf rows contain NaN. I decided to apply 
    # the cuts so NaN are the events recognized as 
    # tau events. 
    edf.where(edf["m2"] < cuts[0], inplace=True)
    edf.where(edf["Q2"] < cuts[1], inplace=True)
    edf.where(edf["Ehash"] > cuts[2], inplace=True)
    edf.where(edf["pt_miss"] < cuts[3], inplace=True)

    N = Nm + Nt
    # una riga vale l'altra, serve a pandas
    # I exploit the fact that 
    # pd.DataFrame.append( ,ignore_index=True, sort=False) 
    # keep the order of the original data frames
    # so muons are in the first Nm rows
    real_muons = edf.m2[:Nm].count()
    real_tau = Nt - edf.m2[Nm:].count()
    reco_muons = edf.m2.count()
    reco_tau = N - reco_muons

    muon_eff = real_muons/Nm
    tau_eff = real_tau/Nt
    muon_pur = real_muons/reco_muons
    tau_pur = real_tau/reco_tau
    return muon_eff, tau_eff, muon_pur, tau_pur

# import data in to a pandas data frame
muon_file = "results-muon_channel.root"
tau_file = "results-tau_channel.root"

muon_rdf = ROOT.RDataFrame('tree', muon_file)
tau_rdf = ROOT.RDataFrame('tree', tau_file)

muon_df = root2pandas(muon_rdf)
tau_df = root2pandas(tau_rdf)
    
# compute the kinematic variables of interest
bc_pt_correct(muon_df, NN=True)
bc_pt_correct(tau_df, NN=True)
 
kinevars(muon_df)
kinevars(tau_df)

# number of expected muons and tau, considering 
# L=100fb-1, the right cross section and BRs
# PER ORA NUMERI TOTALMENTE A CASO
Nm = 1000
Nt = 1000

# cuts to discriminate the channel
m2_cut = 3.0 # GeV^2/c^4
Q2_cut = 7.0 # Gev^2/c^4
Ehash_cut = 1.5 # GeV
pt_miss_cut = 4.0 # GeV/c

cuts = [m2_cut, Q2_cut, Ehash_cut, pt_miss_cut]

muon_eff, tau_eff, muon_pur, tau_pur = pureff(muon_df, tau_df, Nm, Nt, cuts)

print("muon efficiency: ", muon_eff)
print("muon purity: ", muon_pur)
print("tau efficiency: ", tau_eff)
print("tau purity: ", tau_pur)

