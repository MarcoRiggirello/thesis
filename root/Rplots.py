#!/usr/bin/env python

import matplotlib.pyplot as plt
plt.style.use("./thesis.mplstyle")
import numpy as np
import pandas as pd
import ROOT

from Rkinevars import *

def Rroot2pandas(rdf):
    ndf = rdf.AsNumpy(columns=[
        "m2", "Q2", "Ehash", "pt_miss",
        "gen_m2", "gen_Q2", "gen_Ehash", "gen_pt_miss"])
    return pd.DataFrame(ndf)

# import data in to a pandas data frame
muon_file = "results-muon_channel.root"
tau_file = "results-tau_channel.root"

muon_rdf = ROOT.RDataFrame('tree', muon_file)
tau_rdf = ROOT.RDataFrame('tree', tau_file)
    
# manipulation
muon_rdf = Rkinevars(muon_rdf)
tau_rdf = Rkinevars(tau_rdf)

muon_rdf = Rgen_kinevars(muon_rdf)
tau_rdf = Rgen_kinevars(tau_rdf)
    
muon_df = Rroot2pandas(muon_rdf)
tau_df = Rroot2pandas(tau_rdf)

# plotting the results
kwargs = {"alpha": 0.6, "histtype": "stepfilled", "edgecolor": "black"}
mkwargs = {"facecolor": "none", "edgecolor": "#3333b3", "hatch": "///", "label": "$\mu$ cut"}
tkwargs = {"facecolor": "none", "edgecolor": "#3333b3", "hatch": "xxx", "label": "$\\tau$ cut"}

m2_gbins = np.arange(0.0, 7.51, 0.5)
Q2_gbins = np.arange(0.0, 11.0, 0.5)
Ehash_gbins = np.arange(0.0, 5.0, 0.2)
pt_miss_gbins = np.arange(-2.0, 20.0, 1.0)

m2_rbins = np.arange(0.0, 9.1, 1.0)
Q2_rbins = np.arange(0.0, 11.1, 1.0)
Ehash_rbins = np.arange(0.0, 5.0, 0.1)
pt_miss_rbins = np.arange(0.0, 16.1, 2.0)

tau_vs_muon_m2 = pd.DataFrame()
tau_vs_muon_m2["$\mu$ ch."] = muon_df["m2"]
tau_vs_muon_m2["$\\tau$ ch."] = tau_df["m2"]
tau_vs_muon_m2.plot.hist(bins=m2_rbins, **kwargs)
xlim = plt.xlim()
plt.axvspan(6.37, xlim[1], **mkwargs)
plt.axvspan(xlim[0], 0.56, **tkwargs)
plt.legend(framealpha=1, loc="upper right")
plt.xlim(xlim)
plt.title("$m^2(\mu\\text{ ch.}) < 6.37$ GeV$^2$, $m^2(\\tau\\text{ ch.}) > 0.56$ GeV$^2$")
plt.xlabel("$m^2$ [GeV$^2$]")
plt.ylabel("Events / 1 GeV$^2$")

tau_vs_muon_Q2 = pd.DataFrame()
tau_vs_muon_Q2["$\mu$ ch."]= muon_df["Q2"]
tau_vs_muon_Q2["$\\tau$ ch."] = tau_df["Q2"]
tau_vs_muon_Q2.plot.hist(bins=Q2_rbins, **kwargs)
xlim = plt.xlim()
plt.axvspan(10.3, xlim[1], **mkwargs)
plt.axvspan(xlim[0], 2.7, **tkwargs) # ONLY FOR AESTHETIC REASON!!! 
plt.legend(framealpha=1, loc="upper left")
plt.xlim(xlim)
plt.title("$Q^2(\mu\\text{ ch.}) < 10.3$ GeV$^2$, $Q^2(\\tau\\text{ ch.}) > 2.6$ GeV$^2$")
plt.xlabel("$Q^2$ [GeV$^2$]")
plt.ylabel("Events / 1 GeV$^2$")

tau_vs_muon_Ehash = pd.DataFrame()
tau_vs_muon_Ehash["$\mu$ ch."]= muon_df["Ehash"]
tau_vs_muon_Ehash["$\\tau$ ch."] = tau_df["Ehash"]
tau_vs_muon_Ehash.plot.hist(bins=Ehash_rbins, **kwargs)
xlim = plt.xlim()
plt.axvspan(xlim[0], 0.233, **mkwargs)
plt.axvspan(3.494, xlim[1], **tkwargs)
plt.legend(framealpha=1, loc="upper right")
plt.xlim(xlim)
plt.title("$E_\mu^\#(\mu\\text{ ch.}) > 0.233$ GeV, $E_\mu^\#(\\tau\\text{ ch.}) < 3.494$ GeV")
plt.xlabel("$E_\mu^\#$ [GeV]")
plt.ylabel("Events / 100 MeV")

tau_vs_muon_pt_miss = pd.DataFrame()
tau_vs_muon_pt_miss["$\mu$ ch."]= muon_df["pt_miss"]
tau_vs_muon_pt_miss["$\\tau$ ch."] = tau_df["pt_miss"]
tau_vs_muon_pt_miss.plot.hist(bins=pt_miss_rbins, **kwargs)
xlim = plt.xlim()
plt.axvspan(10.9, xlim[1], **mkwargs)
plt.axvspan(xlim[0], 2.3, **tkwargs)
plt.legend(framealpha=1, loc="upper right")
plt.xlim(xlim)
plt.title("$p_T^\\text{miss}(\mu\\text{ ch.}) < 10.9$ GeV, $p_T^\\text{miss}(\\tau\\text{ ch.}) > 2.3$ GeV")
plt.xlabel("$p_T^\\text{miss}$ [GeV]")
plt.ylabel("Events / 2 GeV")

tau_vs_muon_gen_m2 = pd.DataFrame()
tau_vs_muon_gen_m2["$B_c\\to J/\psi(\mu\mu)+\mu\,\\nu$"]= muon_df["gen_m2"]
tau_vs_muon_gen_m2["$B_c\\to J/\psi(\mu\mu)+\\tau_\mu\,\\nu$"] = tau_df["gen_m2"]
tau_vs_muon_gen_m2.plot.hist(bins=m2_gbins, **kwargs)
plt.title("$m^2 = (p_B - p_{\mu_1} - p_{\mu_2} - p_\mu)^2$")
plt.xlabel("$m^2$ [GeV$^2$]")
plt.ylabel("Events / 0.5 GeV$^2$")

tau_vs_muon_gen_Q2 = pd.DataFrame()
tau_vs_muon_gen_Q2["$B_c\\to J/\psi(\mu\mu)+\mu\,\\nu$"]= muon_df["gen_Q2"]
tau_vs_muon_gen_Q2["$B_c\\to J/\psi(\mu\mu)+\\tau_\mu\,\\nu$"] = tau_df["gen_Q2"]
tau_vs_muon_gen_Q2.plot.hist(bins=Q2_gbins, **kwargs)
plt.title("$Q^2 = (p_B - p_{\mu_1} - p_{\mu_2})^2$")
plt.xlabel("$Q^2$ [GeV$^2$]")
plt.ylabel("Events / 0.5 GeV$^2$")
plt.legend(loc="upper left")

tau_vs_muon_gen_Ehash = pd.DataFrame()
tau_vs_muon_gen_Ehash["$B_c\\to J/\psi(\mu\mu)+\mu\,\\nu$"]= muon_df["gen_Ehash"]
tau_vs_muon_gen_Ehash["$B_c\\to J/\psi(\mu\mu)+\\tau_\mu\,\\nu$"] = tau_df["gen_Ehash"]
tau_vs_muon_gen_Ehash.plot.hist(bins=Ehash_gbins, **kwargs)
plt.title("$E_\mu^\# = E_\mu$ nel CM di $\mu_1+\mu_2$")
plt.xlabel("$E_\mu^\#$ [GeV]")
plt.ylabel("Events / 200 MeV")

tau_vs_muon_gen_pt_miss = pd.DataFrame()
tau_vs_muon_gen_pt_miss["$B_c\\to J/\psi(\mu\mu)+\mu\,\\nu$"]= muon_df["gen_pt_miss"]
tau_vs_muon_gen_pt_miss["$B_c\\to J/\psi(\mu\mu)+\\tau_\mu\,\\nu$"] = tau_df["gen_pt_miss"]
tau_vs_muon_gen_pt_miss.plot.hist(bins=pt_miss_gbins, **kwargs)
plt.title("$p_T^\\text{miss} = p_T^B - p_T^{\mu_1} - p_T^{\mu_2} - p_T^\mu$")
plt.xlabel("$p_T^\\text{miss}$ [GeV]")
plt.ylabel("Events / 1 GeV")

gen_vs_reco_muon_m2 = pd.DataFrame() 
gen_vs_reco_muon_m2["gen $m^2$"] = muon_df["gen_m2"]
gen_vs_reco_muon_m2["reco $m^2$"] = muon_df["m2"]
gen_vs_reco_muon_m2.plot.hist(bins=np.arange(0.0, 5.1, 0.2), **kwargs)
plt.title("Gen vs reco $m^2$ -- muon channel")
plt.xlabel("$m^2$ [GeV$^2$]")
plt.ylabel("Events / 0.2 GeV$^2$")
"""
gen_vs_reco_tau_m2 = pd.DataFrame() 
gen_vs_reco_tau_m2["gen"] = tau_df["gen_m2"]
gen_vs_reco_tau_m2["reco"] = tau_df["m2"]
gen_vs_reco_tau_m2.plot.hist(bins=60, alpha=0.6)
plt.title("gen vs reco missing mass sq - tau channel")
plt.xlabel("[GeV^2/c^4]")

gen_vs_reco_muon_Q2 = pd.DataFrame() 
gen_vs_reco_muon_Q2["gen"] = muon_df["gen_Q2"]
gen_vs_reco_muon_Q2["reco"] = muon_df["Q2"]
gen_vs_reco_muon_Q2.plot.hist(bins=60, alpha=0.6, histtype="stepfilled", edgecolor="black")
plt.title("gen vs reco Q2 - muon channel")
plt.xlabel("$Q^2$ [GeV$^2$]")

gen_vs_reco_tau_Q2 = pd.DataFrame() 
gen_vs_reco_tau_Q2["gen"] = tau_df["gen_Q2"]
gen_vs_reco_tau_Q2["reco"] = tau_df["Q2"]
gen_vs_reco_tau_Q2.plot.hist(bins=40, alpha=0.6, histtype="stepfilled", edgecolor="black")
plt.title("gen vs reco Q2 - tau channel")
plt.xlabel("[GeV^2/c^4]")
"""
plt.show()

