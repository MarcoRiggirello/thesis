#!/usr/bin/env python

import numpy as np
import pandas as pd
import ROOT

from Rkinevars import *
from Rcuts import *

# import data
muon_file = "results-muon_channel.root"
tau_file = "results-tau_channel.root"

muon_rdf = ROOT.RDataFrame('tree', muon_file)
tau_rdf = ROOT.RDataFrame('tree', tau_file)

# compute the kinematic variables of interest
muon_rdf = Rkinevars(muon_rdf)
tau_rdf = Rkinevars(tau_rdf)

muon_df = Rroot2pandas(muon_rdf)
tau_df = Rroot2pandas(tau_rdf)

# cuts to discriminate the channel
# the step is the resolution
m2_mcut = np.arange(0.0,9.,1.15) # GeV^2/c^4
Q2_mcut = np.arange(0.0,11.,2.2) # Gev^2/c^4
Ehash_mcut = np.arange(0.0,5.,0.112) # GeV
pt_miss_mcut = np.arange(0.0,15.,4.) # GeV/c

m2_tcut = np.arange(0.0,9.,1.29) # GeV^2/c^4
Q2_tcut = np.arange(0.0,11.,1.5) # Gev^2/c^4
Ehash_tcut = np.arange(0.0,5.,0.054) # GeV
pt_miss_tcut = np.arange(0.0,15.,3.9) # GeV/c

print("Optimization brute force of the figure of merit S/sqrt(S+B) for the 2 channels.")

print("MUON OPTIMIZATION")
i = 0
n = len(m2_mcut)*len(Q2_mcut)*len(Ehash_mcut)*len(pt_miss_mcut)
Sm = 0.0
Smcuts = [0,0,0,0]
for m2 in m2_mcut:
    for Q2 in Q2_mcut:
        for E in Ehash_mcut:
            for p in pt_miss_mcut:
                mcuts = [m2, Q2, E, p]
                sm = mscore(muon_df, tau_df, mcuts)
                if sm > Sm:
                    Sm = sm
                    Smcuts = mcuts
                i = i+1
                if i%100==0:
                    print("cuts evaluated: ",i,"/",n, end="\r")
Np, Nr = mcut(muon_df, Smcuts)
print("cuts evaluated: ",n,"/",n)
print("Muon s max:\t", Sm)
print("with cuts:\t", Smcuts)
print("muon eff:\t", Np/(Np+Nr))

print("TAU OPTIMIZATION")
i = 0
n = len(m2_tcut)*len(Q2_tcut)*len(Ehash_tcut)*len(pt_miss_tcut)
St = 0.0
Stcuts = [0,0,0,0]
for m2 in m2_tcut:
    for Q2 in Q2_tcut:
        for E in Ehash_tcut:
            for p in pt_miss_tcut:
                tcuts = [m2, Q2, E, p]
                st = tscore(muon_df, tau_df, tcuts)
                if st > St:
                    St = st
                    Stcuts = tcuts
                i = i+1
                if i%100==0:
                    print("cuts evaluated: ",i,"/",n, end="\r")
Np, Nr = tcut(tau_df, Stcuts)
print("cuts evaluated: ", n,"/",n)
print("Tau s max:\t", St)
print("with cuts:\t", Stcuts)
print("tau eff:\t", Np/(Np+Nr))

print("R(J/psi) expected (R,DR): ", RJpsi(muon_df, tau_df, Smcuts, Stcuts))

