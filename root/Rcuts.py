#!/usr/bin/env python

import numpy as np
import pandas as pd
import ROOT

def Rroot2pandas(rdf):
    ndf = rdf.AsNumpy(columns=[
        "m2", "Q2", "Ehash", "pt_miss"
        ])
    return pd.DataFrame(ndf)

def mcut(df, cuts):
    """
    For a given pandas df returns the number of
    events that pass the muon cuts and the number 
    of events rejected.
    """
    cf = df.copy()
    cf.where(cf["m2"] < cuts[0], inplace=True)
    cf.where(cf["Q2"] < cuts[1], inplace=True)
    cf.where(cf["Ehash"] > cuts[2], inplace=True)
    cf.where(cf["pt_miss"] < cuts[3], inplace=True)
    Np = cf.m2.count()
    Nr = cf.m2.isna().sum() 
    return Np, Nr

def tcut(df, cuts):
    """
    For a given pandas df returns the number of
    events that pass the tau cuts and the number
    of events rejected.
    """
    cf = df.copy()
    cf.where(cf["m2"] > cuts[0], inplace=True)
    cf.where(cf["Q2"] > cuts[1], inplace=True)
    cf.where(cf["Ehash"] < cuts[2], inplace=True)
    cf.where(cf["pt_miss"] > cuts[3],inplace=True)
    Np = cf.m2.count()
    Nr = cf.m2.isna().sum()
    return Np, Nr

def RJpsi(muon_df, tau_df, muon_cuts, tau_cuts):
    """
    Returns the expected value (and uncertainty) of
    R(J/psi) for a integrated luminosity of 100fb-1
    with the efficiencies estimated with our MC data.
    (Cross section times Branching ratios 
    are estimated by MC too)
    """
    L = 100*1000 #pb-1
    Nm = L*100.0
    Nt = L*4.2
    DNm = L*20.0
    DNt = L*0.9
    Brt = 0.1739 # BR(tau->mu nu nu)
    DBrt = 0.0004
    muon_Np, muon_Nr = mcut(muon_df, muon_cuts)
    tau_Np, tau_Nr = tcut(tau_df, tau_cuts)
    muon_N = muon_Np + muon_Nr
    tau_N = tau_Np + tau_Nr
    muon_eff = muon_Np/muon_N
    tau_eff = tau_Np/tau_N
    muon_Deff = np.sqrt(muon_eff*(1-muon_eff)/muon_N)
    tau_Deff = np.sqrt(tau_eff*(1-tau_eff)/muon_N)
    if (muon_eff == 0) | (tau_eff == 0):
        R = 100.0
        DR = 1000000.0
    else:
        R = (Nt*tau_eff)/(Brt*Nm*muon_eff)
        DR = R*np.sqrt((DNm/Nm)**2 + (DNt/Nt)**2 + (DBrt/Brt)**2 + (muon_Deff/muon_eff)**2 + (tau_Deff/tau_eff)**2)
    return R, DR

def mscore(muon_df, tau_df, cuts):
    """
    Returns the figure of merit 
    Signal/sqrt(Signal + Background)
    as estimator of the goodness of the cuts.
    """
    Sm, x = mcut(muon_df, cuts)
    Bm, x = mcut(tau_df, cuts)
    if Sm + Bm == 0: 
        sm = 0.0
    else:
        sm = Sm/np.sqrt(Sm + Bm)
    return sm

def tscore(muon_df, tau_df, cuts):
    """
    Returns the figure of merit 
    Signal/sqrt(Signal + Background)
    as estimator of the goodness of the cuts.
    """
    St, x = tcut(tau_df, cuts)
    Bt, x = tcut(muon_df, cuts)
    if St + Bt == 0: 
        st = 0.0
    else:
        st = St/np.sqrt(St + Bt)
    return st

