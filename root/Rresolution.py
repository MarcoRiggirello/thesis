#!/usr/bin/env python

import matplotlib.pyplot as plt
plt.style.use("./thesis.mplstyle")
import numpy as np
import pandas as pd
import ROOT

from scipy.stats import moment
from Rkinevars import *

def errorvar(s):
    """
    Takes as input a pandas Series and returns
    the standard deviation and the error of the 
    std deviation.
    """
    N = s.count()
    r = s.std()
    m4 = moment(s, moment=4, nan_policy="omit")
    m2 = moment(s, moment=2, nan_policy="omit")
    DR = (N-1)*((N-1)*m4 - (N-3)*m2**2)/N**3
    return r, np.sqrt(DR)

def Rroot2pandas(rdf):
    """
    takes as input a RDataFrame and returns only the
    columns of interest for theresolution analysis
    """
    ndf = rdf.AsNumpy(columns=[
        "Bc_pt_correct", "gen_b_pt",
        "m2", "Q2", "Ehash", "pt_miss",
        "gen_m2", "gen_Q2", "gen_Ehash", "gen_pt_miss"])
    return pd.DataFrame(ndf)

# import data in to a pandas data frame
muon_file = "results-muon_channel.root"
tau_file = "results-tau_channel.root"

muon_rdf = ROOT.RDataFrame('tree', muon_file)
tau_rdf = ROOT.RDataFrame('tree', tau_file)

# utili strumentopoli
muon_ris = pd.DataFrame()
tau_ris = pd.DataFrame()

# manipulation
print("|---------------------------------------------------------|")
print("|--- RESOLUTION OF THE KINEMATIC VARIABLES OF INTEREST ---|")
print("|---------------------------------------------------------|")
print("|--- All resolutions are given in the raw form (r, Dr) ---|")
print("|--- where Dr is the uncertainty of the resolution r.  ---|")
print("|---------------------------------------------------------|")

## 'manual' pt correction
print("\n     resolutions for 'manual' bc_pt correction \n")

muon_mdf = Rkinevars(muon_rdf, NN=False)
tau_mdf = Rkinevars(tau_rdf, NN=False)

muon_mdf = Rgen_kinevars(muon_mdf)
tau_mdf = Rgen_kinevars(tau_mdf)

muon_df = Rroot2pandas(muon_mdf)
tau_df = Rroot2pandas(tau_mdf)

muon_ris["res_pt"] = (muon_df["Bc_pt_correct"] - muon_df["gen_b_pt"])/muon_df["gen_b_pt"]
tau_ris["res_pt"] = (tau_df["Bc_pt_correct"] - tau_df["gen_b_pt"])/tau_df["gen_b_pt"]
muon_ris["res_m2"] = muon_df["m2"] - muon_df["gen_m2"]
muon_ris["res_Q2"] = muon_df["Q2"] - muon_df["gen_Q2"]
muon_ris["res_Ehash"] = muon_df["Ehash"] - muon_df["gen_Ehash"]
muon_ris["res_pt_miss"] = muon_df["pt_miss"] - muon_df["gen_pt_miss"]
tau_ris["res_m2"] = tau_df["m2"] - tau_df["gen_m2"]
tau_ris["res_Q2"] = tau_df["Q2"] - tau_df["gen_Q2"]
tau_ris["res_Ehash"] = tau_df["Ehash"] - tau_df["gen_Ehash"]
tau_ris["res_pt_miss"] = tau_df["pt_miss"] - tau_df["gen_pt_miss"]

print("bc_pt, muon ch : ", errorvar(muon_ris["res_pt"]), "(relative)")
print("bc_pt, tau ch : ", errorvar(tau_ris["res_pt"]), "(relative)")
print("m2, muon ch: ", errorvar(muon_ris["res_m2"]), "Gev^2/c^4")
print("m2, tau ch: ", errorvar(tau_ris["res_m2"]), "Gev^2/c^4")
print("Q2, muon ch: ", errorvar(muon_ris["res_Q2"]), "Gev^2/c^4")
print("Q2, tau ch: ", errorvar(tau_ris["res_Q2"]), "Gev^2/c^4")
print("E#, muon ch: ", errorvar(muon_ris["res_Ehash"]), "Gev")
print("E#, tau ch: ", errorvar(tau_ris["res_Ehash"]), "Gev")
print("pt_miss, muon ch: ", errorvar(muon_ris["res_pt_miss"]), "Gev/c")
print("pt_miss, tau ch: ", errorvar(tau_ris["res_pt_miss"]), "Gev/c")

## NN pt correction
print("\n     resolutions for the NN corrected bc_pt \n")

muon_Ndf = Rkinevars(muon_rdf)
tau_Ndf = Rkinevars(tau_rdf)

muon_Ndf = Rgen_kinevars(muon_Ndf)
tau_Ndf = Rgen_kinevars(tau_Ndf)

muon_df = Rroot2pandas(muon_Ndf)
tau_df = Rroot2pandas(tau_Ndf)

muon_ris["NN_res_pt"] = (muon_df["Bc_pt_correct"] - muon_df["gen_b_pt"])/muon_df["gen_b_pt"]
tau_ris["NN_res_pt"] = (tau_df["Bc_pt_correct"] - tau_df["gen_b_pt"])/tau_df["gen_b_pt"]
muon_ris["NN_res_m2"] = muon_df["m2"] - muon_df["gen_m2"]
muon_ris["NN_res_Q2"] = muon_df["Q2"] - muon_df["gen_Q2"]
muon_ris["NN_res_Ehash"] = muon_df["Ehash"] - muon_df["gen_Ehash"]
muon_ris["NN_res_pt_miss"] = muon_df["pt_miss"] - muon_df["gen_pt_miss"]
tau_ris["NN_res_m2"] = tau_df["m2"] - tau_df["gen_m2"]
tau_ris["NN_res_Q2"] = tau_df["Q2"] - tau_df["gen_Q2"]
tau_ris["NN_res_Ehash"] = tau_df["Ehash"] - tau_df["gen_Ehash"]
tau_ris["NN_res_pt_miss"] = tau_df["pt_miss"] - tau_df["gen_pt_miss"]

print("bc_pt, muon ch: ", errorvar(muon_ris["NN_res_pt"]), "(relative)")
print("bc_pt, tau ch: ", errorvar(tau_ris["NN_res_pt"]), "(relative)")
print("m2, muon ch: ", errorvar(muon_ris["NN_res_m2"]), "Gev^2/c^4")
print("m2, tau ch: ", errorvar(tau_ris["NN_res_m2"].where(tau_ris["NN_res_m2"]>-20)), "Gev^2/c^4")
print("Q2, muon ch: ", errorvar(muon_ris["NN_res_Q2"]), "Gev^2/c^4")
print("Q2, tau ch: ", errorvar(tau_ris["NN_res_Q2"]), "Gev^2/c^4")
print("E#, muon ch: ", errorvar(muon_ris["NN_res_Ehash"]), "Gev")
print("E#, tau ch: ", errorvar(tau_ris["NN_res_Ehash"]), "Gev")
print("pt_miss, muon ch: ", errorvar(muon_ris["NN_res_pt_miss"]), "Gev/c")
print("pt_miss, tau ch: ", errorvar(tau_ris["NN_res_pt_miss"]), "Gev/c")

# plotting the results (one hist sufficit)
kwargs = {"alpha": 0.6, "histtype": "stepfilled", "edgecolor": "black"}
ptbins = np.arange(-0.55, 0.801, 0.05)

res_muon_pt = pd.DataFrame() 
res_muon_pt["NN"] = muon_ris["NN_res_pt"]
res_muon_pt["``manual''"] = muon_ris["res_pt"]
res_muon_pt.plot.hist(bins=ptbins, **kwargs)
plt.title("NN vs ``manual'' $p_T^B$ correction -- $\mu$ ch. resolution")
plt.xlabel("$r_n(p_T^B)$")
plt.ylabel("Events")

"""
res_tau_pt = pd.DataFrame() 
res_tau_pt["NN"] = tau_df["NN_res_pt"]
res_tau_pt["manual"] = tau_df["res_pt"]
res_tau_pt.plot.hist(bins=60, alpha=0.6)
plt.title("NN vs 'manual' Bc_pt resolution - tau channel")

res_muon_m2 = pd.DataFrame() 
res_muon_m2["NN"] = muon_df["NN_res_m2"]
res_muon_m2["manual"] = muon_df["res_m2"]
res_muon_m2.plot.hist(bins=60, alpha=0.6)
plt.title("NN vs 'manual' m2 resolution - muon channel")
plt.xlabel("GeV^2/c^4")

res_tau_m2 = pd.DataFrame() 
res_tau_m2["NN"] = tau_df["NN_res_m2"]
res_tau_m2["manual"] = tau_df["res_m2"]
res_tau_m2.plot.hist(bins=60, alpha=0.6)
plt.title("NN vs 'manual' m2 resolution - tau channel")
plt.xlabel("GeV^2/c^4")

res_muon_Q2 = pd.DataFrame() 
res_muon_Q2["NN"] = muon_df["NN_res_Q2"]
res_muon_Q2["manual"] = muon_df["res_Q2"]
res_muon_Q2.plot.hist(bins=60, alpha=0.6)
plt.title("NN vs 'manual' Q2 resolution - muon channel")
plt.xlabel("GeV^2/c^4")

res_tau_Q2 = pd.DataFrame() 
res_tau_Q2["NN"] = tau_df["NN_res_Q2"]
res_tau_Q2["manual"] = tau_df["res_Q2"]
res_tau_Q2.plot.hist(bins=60, alpha=0.6)
plt.title("NN vs 'manual' Q2 resolution - tau channel")
plt.xlabel("GeV^2/c^4")
"""

plt.show()


