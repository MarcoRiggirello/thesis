\documentclass[9pt]{beamer}

\useoutertheme{infolines}
\usecolortheme{dolphin}
\usefonttheme{professionalfonts}
\setbeamertemplate{frametitle}[default][center]
\beamertemplatenavigationsymbolsempty
\setbeamerfont{title}{shape=\scshape, size=\Large}
\setbeamerfont{frametitle}{shape=\scshape}

\definecolor{links}{HTML}{3333B3}
\hypersetup{colorlinks, linkcolor=, urlcolor=links}

\usepackage[italian]{babel}

\usepackage{libertinus}

\usepackage{booktabs}
\usepackage{dcolumn}
\newcolumntype{e}[1]{D{+}{\,\pm\,}{#1}}

\usepackage{epsfig}

\graphicspath{{istogrammi/}}

\title[Studio di $R(J/\psi)$]{Studio della violazione dell'universalità del sapore leptonico nei canali $B_c^+\to J/\psi\mu^+\nu_\mu$ e $B_c^+\to J/\psi\tau^+\nu_\tau$ con l'esperimento CMS a LHC}
\author[Marco Riggirello]{Candidato: Marco Riggirello \\ Relatore: prof. Guido Emilio Tonelli}
\institute[Fis UNIPI]{Dipartimento di Fisica E.~Fermi, Università di Pisa}
\titlegraphic{\includegraphics[width=0.1\linewidth]{cherubino-black.eps}}
\date{17 settembre 2020}

\begin{document}

\frame{\titlepage}

\begin{frame}{Introduzione}
	Universalità del sapore leptonico (LFU): simmetria del Modello Standard che prevede uguale accoppiamento dei leptoni ai bosoni vettori. Un test della LFU è la misura del rapporto
			\begin{equation}
				R(J/\psi) = \frac{\symcal{B}(B_c^+\to J/\psi\tau^+\nu_\tau)}{\symcal{B}(B_c^+\to J/\psi\mu^+\nu_\mu)}
			\end{equation}
	\begin{itemize}
		\item l'esperimento \textit{LHCb} ha misurato $R(J/\psi)=0.71\pm0.17\,\text{(stat)}\pm\,0.18\,\text{(syst)}$, in tensione con il valore atteso (0.25--0.28): interessante una verifica di questo risultato con CMS;
		\item la misura è non banale poiché 
			\begin{equation*}
				J/\psi\to\mu^+\mu^- \qquad \tau^+\to\mu^+\bar{\nu}_\tau\nu_\mu
			\end{equation*}
	\end{itemize}
			
	Si vuole studiare con quale precisione è possibile misurare $R(J/\psi)$ con CMS.
	Usando pyROOT, pandas, numpy e scipy.optimize vengono analizzati dati Monte Carlo (2000 eventi per canale) forniti dalla collaborazione CMS. Due tipi di variabili cinematiche:
	\begin{itemize}
		\item generate: il valore previsto dalla teoria;
		\item ricostruite: si simula il rumore introdotto dal CMS.
	\end{itemize}
Non viene studiato il fondo dei due canali. 
\end{frame}

\begin{frame}{Discriminazione dei canali}
	\begin{figure}
		\centering
		\includegraphics[width=.45\textwidth]{gen-m2.pdf}
		\includegraphics[width=.45\linewidth]{gen-Q2.pdf}\\
		\includegraphics[width=.45\linewidth]{gen-Ehash.pdf}
		\includegraphics[width=.45\linewidth]{gen-ptmiss.pdf}
	\end{figure}
	\begin{equation*}
		\text{Evento }^\mu_\tau \in \{ m^2 \lessgtr C[0]^\mu_\tau \,\land\, Q^2 \lessgtr C[1]^\mu_\tau \,\land\, E_\mu^\# \gtrless C[2]^\mu_\tau \,\land\, p_T^\text{miss} \lessgtr C[3]^\mu_\tau \}
	\end{equation*}
\end{frame}

\begin{frame}{Ricostruzione $p_T^{B_c}$ e risoluzioni}
	\begin{figure}
		\centering
		\includegraphics[width=.45\linewidth]{bc-pt-res.pdf}
		\includegraphics[width=.45\textwidth]{genvsreco-m2.pdf}
	\end{figure}
	\begin{center}
		\emph{Risoluzione:} deviazione standard di $r(\kappa)=(\kappa_\text{reco} - \kappa_\text{gen})$ oppure $r_n(\kappa) = (\kappa_\text{reco} - \kappa_\text{gen})/\kappa_\text{gen}$. 
	\end{center}
	\begin{table}
		\centering
		\small
		\begin{tabular}{l e{-3}e{-3} e{-3}e{-3}}
			\toprule
			Correzione $p_T^{B_c^+}$ &\multicolumn{2}{c}{``Manuale'': $p_T^{3\mu}\,[m_{B_c^+}/M(3\mu)]$} & \multicolumn{2}{c}{Rete neurale} \\
			\cmidrule(lr){2-3} \cmidrule(lr){4-5}
			& \multicolumn{1}{c}{Canale $\mu$} & \multicolumn{1}{c}{Canale $\tau$} & \multicolumn{1}{c}{Canale $\mu$} & \multicolumn{1}{c}{Canale $\tau$} \\
			\midrule
			$p_T^B$ [\%] & 18.2+0.1 & 16.91+0.09 & 13.21+0.08 & 15.03+0.07 \\
			$m^2$ [GeV$^2$] & 1.61+0.08 & 1.36+0.07 & 1.15+0.06 & 1.29+0.06 \\
			$Q^2$ [GeV$^2$] & 2.4+0.2 & 1.59+0.08 & 2.2+0.1 & 1.5+0.2 \\
			$E_\mu^\#$ [MeV] & 112+5 & 54+1 & 112+5 & 54+1 \\
			$p_T^\text{miss}$ [GeV] & 4+1 & 4.3+0.8 & 4+1 & 3.9+0.7 \\
			\bottomrule
		\end{tabular}
	\end{table}
\end{frame}

\begin{frame}{Tagli cinematici}
	Si definiscono
	\begin{gather*}
		\symcal{N}_\tau = L\,\,\sigma(B_c^+)\,\symcal{B}(B_c^+\to J/\psi\tau^+\nu_\tau)\,\symcal{B}(J/\psi\to\mu^+\mu^-)\,\symcal{B}(\tau^+\to\mu^+\bar{\nu}_\tau\nu_\mu)\big\rvert_{p_T^B>10\,\text{GeV},\,\lvert y^B \rvert < 2.1} \\
		\symcal{N}_\mu = L\,\,\sigma(B_c^+)\,\symcal{B}(B_c^+\to J/\psi\mu^+\nu_\mu)\,\symcal{B}(J/\psi\to\mu^+\mu^-)\big\rvert_{p_T^B>10\,\text{GeV},\,\lvert y^B \rvert < 2.1} \\
		\epsilon_\mu = \frac{N_\mu^\text{cut}}{N_\mu} \qquad
		\epsilon_\tau = \frac{N_\tau^\text{cut}}{N_\tau} \qquad
		\left[ \Delta\epsilon = \sqrt{\frac{\epsilon(1-\epsilon)}{N}}\right]
	\end{gather*}
	da cui
	\begin{equation}
		R(J/\psi) = \frac{1}{\symcal{B}(\tau^+\to\mu^+\bar{\nu}_\tau\nu_\mu)}\frac{\symcal{N}_\tau \epsilon_\tau}{\symcal{N}_\mu \epsilon_\mu}
	\end{equation}
	con
	\begin{align*}
		L &= 100\,\text{fb}^{-1} \\
		\symcal{B}(\tau^+\to\mu^+\bar{\nu}_\tau\nu_\mu) &= 17.39\pm0.04\,\% \\
		\sigma(B_c^+)\,\symcal{B}(B_c^+\to J/\psi\tau^+\nu_\tau)\,\symcal{B}(J/\psi\to\mu^+\mu^-)\,\symcal{B}(\tau^+\to\mu^+\bar{\nu}_\tau\nu_\mu)\big\rvert_{p_T^B>10\,\text{GeV},\,\lvert y^B \rvert < 2.1} &= (4.2\pm0.9)\,\text{pb} \\
		\sigma(B_c^+)\,\symcal{B}(B_c^+\to J/\psi\mu^+\nu_\mu)\,\symcal{B}(J/\psi\to\mu^+\mu^-)\big\rvert_{p_T^B>10\,\text{GeV},\,\lvert y^B \rvert < 2.1} &= (100\pm20)\,\text{pb}
	\end{align*}
	I tagli sulle variabili cinematiche si ottimizzano per minimizzare $\Delta R/R$.
\end{frame}

\begin{frame}{Risultati}
	\begin{equation}
		{\scriptstyle \epsilon_\mu = 0.999\pm0.001} \qquad
		\boxed{R(J/\psi) = 0.24\pm0.07} \qquad
		{\scriptstyle \epsilon_\tau = 0.997\pm0.001}
	\end{equation}
	\begin{figure}
		\vspace{-5pt}
		\centering
		\includegraphics[width=.45\textwidth]{reco-m2.pdf}
		\includegraphics[width=.45\linewidth]{reco-Q2.pdf}\\
		\includegraphics[width=.45\linewidth]{reco-Ehash.pdf}
		\includegraphics[width=.45\linewidth]{reco-ptmiss.pdf}
		\vspace{-5pt}
	\end{figure}
	{\small Il valore di $R(J/\psi)$ ottenuto dall'ottimizzazione dei tagli appare molto promettente ai fini della misura.}

\end{frame}

\begin{frame}{Colophon}
	Per i riferimenti bibliografici, i dati Monte Carlo, i programmi di analisi e ottimizzazione e per questa presentazione consultare la \emph{repository}
	\begin{center}
		\url{https://gitlab.com/MarcoRiggirello/thesis}
	\end{center}

	Si ringrazia il prof.~G.E.~Tonelli per aver reso possibile questo lavoro e per i tanti consigli. 
	Si ringraziano vivamente i dott.ri F.~Palla e G.R.~Sanchez della collaborazione CMS per aver fornito i dati Monte Carlo, i valori della sezione d'urto di produzione dei processi e, soprattutto, per il prezioso aiuto e i numerosi suggerimenti durante tutte le fasi di analisi dati.
\end{frame}

\end{document}

