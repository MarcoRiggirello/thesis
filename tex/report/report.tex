% vim: set wrap
% vim: set texwidth=72
\documentclass[twocolumn, a4paper]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage[italian]{babel}

%%% BELLURIE

% Font
\usepackage[mono=false]{libertine}
\usepackage{libertinust1math} %AMS caricato in automatico

\usepackage{microtype, booktabs, subcaption}

% Frontespizio
\usepackage{titling}

%titolo
\pretitle{\begin{center}\scshape\sffamily\huge}
\posttitle{\par\end{center}}
%autore
\preauthor{\begin{center}\large\scshape\sffamily\begin{tabular}[t]{c}}
\postauthor{\end{tabular}\par\end{center}}
%data
\predate{\begin{center}\large\sffamily}
\postdate{\par\end{center}}

%titoli sezioni in sans
\usepackage[sf, pagestyles]{titlesec}

%intestatzione e pié di pagina
\renewpagestyle{plain}[\scshape\sffamily]{\sethead{Dipartimento di Fisica E.~Fermi, Universit\`a di Pisa}{}{Bozza tesi triennale}\setfoot{\theauthor}{}{\thepage}}

\pagestyle{plain}

% elenchi puntati meno rientrati
\usepackage{enumitem}
\setlist{leftmargin=*}

% Didascalie
\usepackage{caption}
\captionsetup{tableposition=top,figureposition=bottom,font=small,labelfont={sf, sc}, textfont=sf}

%%% GRAFICI & TABELLE

\usepackage[separate-uncertainty]{siunitx}
%ricordati [table-format=1.6e2]

\usepackage{pgfplots}
\pgfplotsset{compat=1.14}

\usepackage{pgfplotstable} 

\usepackage{tikzscale, rotating}

\usepackage{graphicx}

\usepackage[math]{blindtext}
%%%
%%%
%%%

\begin{document}

\title{Studio della violazione dell'universalità del sapore leptonico
nei canali $B_c^+ \to J/\psi \mu^+ \nu_\mu$ e $B_c^+ \to J/\psi \tau^+
\nu_\tau$ con l'esperimento CMS a LHC}
\author{Marco Riggirello}
\maketitle

\begin{abstract}
	solo un breve rapporto non troppo formale ma (si spera)
	piuttosto chiaro sulla mia analisi dati. [~] indica le citazioni
	che mancano ad articoli letti. Nelle note in calce i dubbi.
\end{abstract}

\section{Scopo dell'analisi}
La misura del rapporto di decadimento 
\begin{equation}
	R(J/\psi) \equiv \frac{\mathcal{B}(B_c^+\to J/\psi \mu^+
	\nu_\mu)}{\mathcal{B}(B_c^+\to J/\psi \tau^+ \nu_\tau)}
\end{equation}
è un'importante verifica del modello standard, che prevede
$R(J/\psi)=1$ [~]. Nell'esperimento LHCb è stata una deviazione dal
valore atteso del modello standard di 2.1$\sigma$ [~], abbastanza per
rendere interessante uno studio su questi decadimenti anche presso il
CMS. Si vuole quindi capire con che incertezza è possibile misurare
$R(J/\psi)$ data la luminosità integrata che è possibile raggiungere e
quale sia il valore centrale atteso.

\section{Svolgimento dell'analisi}
\subsection{Considerazioni preliminari}
Siccome $J/\psi \to \mu^+ \mu^-$ e $\tau^+ \to \mu^+ \bar{\nu}_\tau
\nu_\mu$ [~] si ha che 
\begin{align*}
	B_c^+ & \to J/\psi \mu^+\nu_\mu \to \mu^+ \mu^- \mu^+ \nu_\mu \\
	B_c^+ & \to J/\psi \tau^+ \nu_\mu \to \mu^+ \mu^- \mu^+ \nu_\mu
	\bar{\nu}_\tau \nu_\tau
\end{align*}
Siccome l'apparato è totalmente trasparente ai neutrini, riveleremo in
entrambi i casi solamente una terna di muoni; non potendo affidarci allo
stato finale del sistema, dovremo basare la discriminazione dei due
canali di decadimento sulla 
distribuzione di alcune variabili cinematiche che sarà diversa a causa
del diverso numero di particelle e della diversa massa del muone e del
tau.
Tutta la trattazione della discriminazione dei due canali dal fondo di
altri processi non è argomento che viene trattato in questa analisi.

\subsection{Cosa abbiamo}
Vengono forniti due file root, uno per ciascun canale, contenenti le
simulazioni Montecarlo dei processi di nostro interesse. Di tutte le 
informazioni ivi contenute (in unità naturali) quelle utilizzate per 
il nostro lavoro sono le componenti spaziali~\footnote{salvate come 
$p_T$, $\eta$ e $\phi$. L'energia a quanto pare viene mal ricostruita
ed è meglio costruire la componente temporale a partire dall'impulso 
e dalla massa a riposo (nota). Tutto ha a che fare con come è fatto 
il CMS, da capire quanto si debba e possa capire.} dei quadrimpulsi 
dei tre muoni, i quali sono distinti in muone \emph{disaccoppiato} e 
la coppia che viene dal decadimento del $J/\psi$~\footnote{assai 
probabilmente vengono ricostruiti tramite un fit al vertice di 
decadimento, dovrei riguardare.}. Essendo frutto di una simulazione 
Montecarlo abbiamo due tipi di variabile: quella generata e quella 
ricostruita. Quelle generate contengono i valori esatti, propri del 
decadimento così come è avvenuto; quelle ricostruite aggiungono tutto 
il rumore introdotto dall'apparato sperimentale (\emph{Bremmstrahlung},
intervallo di rapidità finito, \dots). Sbirciando in letteratura [~] 
si vede che le quantità cinematiche di maggior interesse (poiché sono
quelle che più si diversificano tra i due canali) sono:
\begin{align}\label{eqn:vars}
	m_\text{miss}^2 &= (p_B - p_{\mu_1} - p_{\mu_2} - p_\mu)^2 \\
	Q^2 &= (p_B - p_{\mu_1} - p_{\mu_2})^2 \\
	p_T^\text{miss} &= (p_T^B - p_T^{\mu_1} - p_T^{\mu_2} -
	p_T^\mu) \\
	E_\mu^\# &= E_\mu\,\text{nel sys. rif. del CM degli altri due
	$\mu$}
\end{align}
dove $p$ indica il quadrimpulso, $p_T$ l'impulso trasverso, $E$
l'energia, $B$ sta per $B_c^+$ $\mu$ per il muone disaccoppiato e
$\mu_1,\,\mu_2$ sono i muoni provenienti dal decadimento di $J/\psi$.

Effettivamente se si guardano le distribuzioni dalle variabili
generate è così, sono belle diverse, il problema arriva alle variabili
ricostruite: $B_c$ ha una vita troppo breve per poter essere rivelato,
bisogna affidarsi ai tre muoni, quindi il quadrimpulso del nostro amico
va ricostruito a partire da quello dei tre mu. I nostri file montecarlo
contengono anche una ricostruzione del $p_T$ a partire dal $p_T$ del
sistema dei tre muoni, effettuata tramite l'ultilizzo di una rete
neurale: per i nostri scopi possiamo anche limitarci a ringraziare chi
ha fatto il lavoro senza chiedersi troppo come ha fatto.

\subsection{Cosa ci facciamo}

\subsubsection{Ricostruzione del $p_T^B$} \label{subsubsec:pt}
Per avere il corretto quadrimpulso per il $B_c^+$, necessario per le
variabili cinematiche in esame, bisogna correggere il $p_T$. La
letteratura suggerisce due metodi[~]~[~]: 
\begin{itemize}
	\item usare la correzione $p_T^B = p_T^{3\mu} M(B_c^+)/M(3\mu)$,
		dove $p_T^{3\mu}$ e $M(3\mu)$ indicano rispettivamente
		l'impulso trasverso e la massa invariante del sistema
		dei 3 muoni e $M(B_c^+)$ la massa a riposo del mesone;
	\item correggere tramite una rete neurale studiata per lo scopo.
\end{itemize}
Avendo possibilità di usare entrambi i metodi, verificheremo qual è il
migliore. Intanto notiamo visivamente (\emph{istogrammi!!!}) che alcune
distribuzioni ricostruite sono piuttosto diverse da quelle generate, una
su tutte la distribuzione di $m^2$ per il canale di decadimento $\mu$
che nel caso generato è fortemente piccata sopra lo zero ovvero,
chiaramente, la massa invariante dell'unico neutrino presente nel 
decadimento mentre nella controparte ricostruita si ha una distribuzione
più larga: pare che ciò sia dovuto alla \emph{bremmstrahlung} dei muoni
nei vari calorimetri, ma credo che sia necessaria una citazione per
giustificare questa giustificazione così tanto giustificabile ma così
poco giustificata.

\subsubsection{Calcolo delle risoluzioni}
Si calcola per ogni evento del set di dati, data la variabile cinematica
$v$, la quantità $r= v_\text{reco} - v_\text{gen}$ oppure $r_n
= (v_\text{reco} - v_\text{gen})/v_\text{gen}$ dove "reco" indica la
variabile ricostruita e "gen" quella generata; la deviazione standard
della distribuzione di $r$ e $r_n$ prende il nome di \emph{risoluzione}
(normalizzata, nel secondo caso) ed è stata calcolata per la correzione
al $p_T$ del $B_c^+$ e per le variabili cinematiche di cui sopra
usando entrambe le ricostruzioni indicate in §\ref{subsubsec:pt}.

Le risoluzioni per entrambi i metodi di ricostruzione con le relative 
incertezze sono riportate nel file \texttt{root/resolution.txt}, non ho
la minima voglia per ora di riscrivere tutto in \LaTeX per ora, quando
sarà da fare la bella copia si vedrà. In ogni caso, si hanno risoluzioni
migliori per la correzione con la rete neurale, come
atteso.~\footnote{anche se la rete neurale porta qualche outlier/numero
totalmente fuori di ogni ragionevolezza (si veda per esempio l'errore sulla
risoluzione di $m^2_\tau$) che ho prontamente tolto dall'analisi, ma 
senza un ragionamento rigoroso.} Qualche grafichino non sarebbe male, 
poi si vede.

\subsubsection{Calcolo di efficienza e purezza}
D'ora in poi si usa la ricostruzione con la rete neurale, che si è
dimostrata più accurata. Vogliamo capire come, a partire da meri tagli
cinematici, sia possibile distinguere i due diversi canali di
decadimento e con che precisione.
Si definisce \emph{efficienza} $\epsilon_{[\mu|\tau]}$ di un dato
canale 
\begin{equation*}
	\epsilon_{[\mu|\tau]}=\frac{N_{[\mu|\tau]}^\text{cut}}{N_{[\mu|\tau]}}
\end{equation*}	
dove $N_{[\mu|\tau]}^\text{cut}$ indica il numero di eventi del canale che 
superano i tagli e $N_{[\mu|\tau]}$ il numero totale di eventi di quel
canale effettivamente avvenuti mentre si definisce
\emph{purezza} del segnale 
\begin{equation*}
	\frac{N_{[\mu|\tau]}^\text{cut}}{N^\text{cut}}
\end{equation*}
dove il denominatore indica tutti gli eventi che hanno passato i tagli
per un dato canale, e quelli del canale e quelli dell'altro canale;
questi ultimi in questo caso sono da considerarsi come mero "fondo".

\subsubsection{Calcolo e ottimizzazione di $\Delta R(J/\psi)$}
Se si assume che LHC produca eventi per una luminosità integrata di 
100~fb$^{-1}$ si ha, conoscendo la sezione d'urto della produzione di
$B_c^+$, e i rapporti di decadimento dei vari processi in gioco si ha
che il numero totale di eventi del canale $\mu$ e del canale $\tau$ ci
si attende che siano, rispettivamente,
\begin{align*}
	N_\mu = L \sigma(B_c^+) \text{BR}(B_c^+  \to J/\psi
	\mu^+\nu_\mu)& \text{BR}(J/\psi \to \mu^+ \mu^-) \\
	N_\tau = L \sigma(B_c^+) \text{BR}(B_c^+ \to J/\psi
	\tau^+ \nu_\tau)& \text{BR}(J/\psi \to \mu^+ \mu^-) \\
	&\text{BR}(\tau^+ \to \mu^+ \bar{\nu}_\tau \nu_\mu)
\end{align*}
mentre quelli rilevati dall'esperimento sono~\footnote{Non ho ancora
capito come la mia conoscenza della purezza possa/debba essere sfruttata a
questo punto. Come tengo conto dell'errore introdotto dal fondo? Come si
modifica $R(J/\psi)$, ovvero come si modificano $N_\mu$ e $N_\tau$?}
\begin{align*}
	N_\mu^\text{cut} &= N_\mu \epsilon_\mu \\
	N_\tau^\text{cut} &= N_\tau \epsilon_\tau
\end{align*}
e, chiaramente, si misurerà il rapporto
\begin{equation*}
	R \equiv \frac{N_\mu^\text{cut}}{N_\tau^\text{cut}}
\end{equation*}

Si voglioni ottimizzare i tagli cinematici di modo che per gli $N_\mu$ e
$N_\tau$ attesi le efficienze siano massime e l'errore su $R$ sia
minimo.

\emph{continua\dots}

\end{document}

