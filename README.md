# PHYSICS BACHELOR THESIS

## CONTENTS

- `root/` : data and scripts for analysis;
- `root/legacy/` : the old pandas-centered implementation of the analysis;
- `tex/longabstract/` : a longabstract to submit 10 days before the dissertation;
- `tex/presentazione/` : talk slides;
- `tex/presentazione/backup/` : backup slides for extra explanation;
- `tex/report/` : a small, informal summary of my analysis, not for the dissertation.

## REFERENCES

https://arxiv.org/abs/1506.08614

https://arxiv.org/abs/1711.05623

https://arxiv.org/abs/2007.06956

https://arxiv.org/abs/2007.08481
